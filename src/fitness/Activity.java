/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fitness;


/**
 *
 * @author AL-Hussein Soliman
 */
public class Activity {
   

    public Activity() {
    }
    private float hrateinc;
    private int actCalories;
    private float totalHrateInc ;
    private int cburnt;
    private String name;
    private int cBurnt;
    private float hrincrease;
    
    public float getTotalHrateInc() {
        return totalHrateInc;
    }
    
    
        //Constructor
    public Activity(int cburnt, float hrateinc) {
        this.cburnt = cburnt;
        this.hrateinc = hrateinc;
    }
    
    public Activity(String name, int cBurnt, float hrincrease) {
    this.name = name;
    this.cBurnt = cBurnt;
    this.hrincrease = hrincrease;
    }

    
    public void setTotalHrateInc(float totalHrateInc) {
        this.totalHrateInc = totalHrateInc;
    }

    
    public int getCburnt() {
        return cburnt;
    }

    
    public int getActCalories() {
        return actCalories;
    }

    
    public void setActCalories(int actCalories) {
        this.actCalories = actCalories;
    }

    
    public void setCburnt(int cburnt) {
        this.cburnt = cburnt;
    }

    
    public float getHrateinc() {
        return hrateinc;
    }
    
    public void setHrateinc(float hrateinc) {
        this.hrateinc = hrateinc;
    }

}


