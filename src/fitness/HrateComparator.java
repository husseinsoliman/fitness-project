
package fitness;

import java.util.Comparator;

public class HrateComparator implements Comparator<SortedAct> {
    @Override
    public int compare(SortedAct o1, SortedAct o2) {
        if(o1.getHrincrease() == o2.getHrincrease())return 0;
        if(o1.getHrincrease() > o2.getHrincrease())return 1;
        else return -1;
    }
}
