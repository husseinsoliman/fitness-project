
package fitness;

import javafx.application.Application ;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;
import javafx.scene.layout.VBox;

public class Main extends Application  {
    Stage window;
    Button button;
   
     
    public static void main(String[] args) {
        launch(args);
    }
    
    @Override
    public void start(Stage primaryStage) throws Exception {
       window = primaryStage;
       window.setTitle("Simple Fitness Tracker");
       
        
           //Creating four buttons for the result , adding another activity and closing program at input scene and at output scene
       Button result = new Button("Result");
       Button addAct = new Button("Add activity");
       Button cancel = new Button("Cancel");
       Button done = new Button ("Done");
       
       
           //Creating five labels for understanding   
       Label label1 = new Label("Choose the type of activity");
       Label label2 = new Label("Enter the time taken to practice in minutes");
       Label label3 = new Label("Total calories burnt");
       Label label4 = new Label("Heart rate at the end");
       Label label5 = new Label("Activities Rank");
       
       
           //Creating choice box for choosing the activity
       ChoiceBox<String> choiceBox = new ChoiceBox<>();
       choiceBox.getItems().addAll("SWIMMING" , "RUNNING" , "KICK_BOXING" , "STRENGTH_TRAINING");
       choiceBox.setPrefWidth(180);
       choiceBox.setMaxWidth(180);
       choiceBox.setValue("");
       

           //Creating three textfields for time , total calories burnt and total heart rate 
       TextField timeTxt = new TextField();
       TextField caloriesOutput = new TextField();
       TextField heartRateOutput = new TextField();
       timeTxt.setPrefWidth(150);
       timeTxt.setMaxWidth(150);
       timeTxt.setAlignment(Pos.CENTER);
       caloriesOutput.setPrefWidth(170);
       caloriesOutput.setMaxWidth(170);
       caloriesOutput.setDisable(false);
       caloriesOutput.setAlignment(Pos.CENTER);
       heartRateOutput.setPrefWidth(200);
       heartRateOutput.setMaxWidth(200);
       heartRateOutput.setDisable(false);
       heartRateOutput.setAlignment(Pos.CENTER);
       
           //Creating listview for listing the activities sorted which the user has entered
       ListView<String> ActSortedList = new ListView<>();
       ActSortedList.setPrefWidth(400);
       ActSortedList.setMaxWidth(400);
       
       
           //All Layouts
           //First creating the input scene hbox for the bottom of the borderpane , hbox has three buttons
       HBox hbx1 = new HBox();
       hbx1.getChildren().addAll(addAct , result , cancel);
       hbx1.setSpacing(25);
       hbx1.setAlignment(Pos.CENTER);
    
           //Then creating the input scene vbox for the center of the borderpane , vbox has the time textfield and the choice box with their labels 
       VBox vbx1 = new VBox();
       vbx1.getChildren().addAll(label1 , choiceBox , label2 , timeTxt );
       vbx1.setPadding(new Insets(80));
       vbx1.setSpacing(30);
       vbx1.setAlignment(Pos.CENTER);
       
           //Creating borderpane for the input scene to have the hbox and vbox
       BorderPane border1 = new BorderPane();
       border1.setPadding(new Insets (0 , 0 ,30, 10));
       border1.setBottom(hbx1);
       border1.setCenter(vbx1);

           //Creating the output scene vbox which has the output textfields with thier labels , the sorted list and the done button
       VBox vbx2 = new VBox();
       vbx2.getChildren().addAll(label3 , caloriesOutput , label4 , heartRateOutput , label5 , ActSortedList , done);
       vbx2.setPadding(new Insets(-30));
       vbx2.setSpacing(25);
       vbx2.setAlignment(Pos.CENTER);
       vbx2.setPadding(new Insets(30));
       
       
           //Creating the scenes
       Scene inputScene = new Scene(border1 , 470 , 410);
       Scene outputScene = new Scene(vbx2 , 400 , 653);
      
       
           //An object of class Calculations to call the buttons methods in Calculations class
       Calculations b = new Calculations();
      
           //Calling buttons methods      
       addAct.setOnAction(e -> {
           b.AddActivity(choiceBox, timeTxt, caloriesOutput, heartRateOutput );
               });
       
       result.setOnAction(e -> {
           b.Result(choiceBox, timeTxt, caloriesOutput, heartRateOutput  , outputScene , window , ActSortedList , b);
           window.setScene(outputScene);
               });
       
       cancel.setOnAction(e ->closeProgram(window));
       done.setOnAction(e -> window.close());
       
           
           //Showing the window
       window.setScene(inputScene);
       window.show();
       
           //Request of Window Closing by button X not Cancel
       window.setOnCloseRequest(e -> {
               e.consume();
               closeProgram(window);
       });
       
    }
     private static void closeProgram(Stage mainwindow){
            boolean answer = ConfirmBox.display("Exit Program", "Are you sure you want to exit program?");
            if(answer) mainwindow.close();
        }
     
}
