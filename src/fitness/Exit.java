/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fitness;

import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 *
 * @author AL-Hussein Soliman
 */
public class Exit {
  public static void display (Stage mainwindow){  
        //Primary Stage
    Stage exitwindow = new Stage();
    exitwindow.setOnCloseRequest(e -> exitwindow.close());
    exitwindow.initModality(Modality.APPLICATION_MODAL);
    exitwindow.setTitle("Exit Program");
    
    
        //Label and two buttons to be sure to exit
    Label label = new Label("Are you sure you want to exit program");
    Button yesbutton = new Button("Yes");
     yesbutton.setOnAction(e ->{ 
        exitwindow.close();
        mainwindow.close();
            });
    Button nobutton = new Button("No");
    nobutton.setOnAction(e -> exitwindow.close());
   
    
        //Layout
    HBox hbx = new HBox();
    hbx.getChildren().addAll(yesbutton , nobutton);
    hbx.setAlignment(Pos.CENTER);
    hbx.setSpacing(40);
    
    VBox vbx = new VBox(10);
    vbx.setAlignment(Pos.CENTER);
    vbx.setSpacing(20);
    vbx.getChildren().addAll(label , hbx);
    
    
        //Scene
    Scene scene = new Scene(vbx , 330 , 120);
    
        //Window Showing
    exitwindow.setScene(scene);
    exitwindow.showAndWait();
            }
    
}

    

