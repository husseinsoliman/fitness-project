
package fitness;

import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class ConfirmBox {
    static boolean answer;
    
    public static boolean display (String text , String message){  
        //Primary Stage
    Stage exitwindow = new Stage(); 
    exitwindow.initModality(Modality.APPLICATION_MODAL);
    exitwindow.setTitle(text);
    
    
        //Label and two buttons to be sure to exit
    Label label = new Label(message);
    Button yesbutton = new Button("Yes");
     yesbutton.setOnAction(e ->{ 
         answer = true;
        exitwindow.close();
            });
     
    Button nobutton = new Button("No");
    nobutton.setOnAction(e ->{
        answer = false;
        exitwindow.close(); 
    });
   
    
        //Layout
    HBox hbx = new HBox();
    hbx.getChildren().addAll(yesbutton , nobutton);
    hbx.setAlignment(Pos.CENTER);
    hbx.setSpacing(40);
    
    VBox vbx = new VBox(10);
    vbx.setAlignment(Pos.CENTER);
    vbx.setSpacing(20);
    vbx.getChildren().addAll(label , hbx);
    
    
        //Scene
    Scene scene = new Scene(vbx , 330 , 120);
    
        //Window Showing
    exitwindow.setScene(scene);
    exitwindow.showAndWait();
    
        //Return Statement
    return answer;
            }
    
}