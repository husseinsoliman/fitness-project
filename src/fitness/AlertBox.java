/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fitness;

import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.stage.Stage;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;

public class AlertBox {

    public static void display(String text, String message, float a, float b) {
        //Primary Stage
        Stage window = new Stage();
        window.initModality(Modality.APPLICATION_MODAL);
        window.setTitle(text);

        //Label for showing the message and Button for closing the window
        Label label = new Label(message);
        Button button = new Button("OK");
        button.setOnAction(e -> window.close());

        //Layout
        VBox layout = new VBox(10);
        layout.setAlignment(Pos.CENTER);
        layout.getChildren().addAll(label, button);

        //Scene
        Scene scene = new Scene(layout, a, b);

        //Window Showing
        window.setScene(scene);
        window.showAndWait();
    }

}
