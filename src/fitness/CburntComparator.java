
package fitness;

import java.util.Comparator;

public class CburntComparator implements Comparator<SortedAct> {
    @Override
    public int compare(SortedAct o1, SortedAct o2) {
       if(o1.getcBurnt() == o2.getcBurnt())return 0;
        if(o1.getcBurnt() > o2.getcBurnt())return 1;
        else return -1;
    }

}