
package fitness;


import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import javafx.scene.Scene;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

public class Calculations {
    int time = 0;
    float orate = 80f;
    float nrate ;
    int calories;
    int sumcalories = 0;
    float h1=0f , h2=0f , h3=0f , h4=0f;
    int s1=0,s2=0,s3=0,s4=0;
   
           //All Objects from class Activity
    Activity SWIMMING = new Activity (4 , 0.2f);
    Activity RUNNING = new Activity (5 , 0.3f);
    Activity KICK_BOXING = new Activity (3 , 0.5f);
    Activity STRENGTH_TRAINING = new Activity (5 , 0.6f);
    
           //Buttons Methods
    public void AddActivity(ChoiceBox<String> choiceBox , TextField txt1 , TextField txt2 , TextField txt3 ){
           
        if("".equals(choiceBox.getValue())) AlertBox.display("Error", "Please choose your activity" , 250 , 100);
        else if(isInt(txt1)){
        if(isPositive(txt1)){
            time = Integer.parseInt(txt1.getText());
            
   
        if( "SWIMMING".equals(getChoice(choiceBox)) ){
            nrate = orate + orate*time*(SWIMMING.getHrateinc())/100;
            h1 += orate*time*(SWIMMING.getHrateinc())/100;
            SWIMMING.setTotalHrateInc(h1);
            orate = nrate;
            calories = SWIMMING.getCburnt()*time;
            sumcalories += calories;
            s1 += SWIMMING.getCburnt()*time;
            SWIMMING.setActCalories(s1) ;            
        }
        
        else if("RUNNING".equals(getChoice(choiceBox))){
            nrate = orate + orate*time*(RUNNING.getHrateinc()/100);
            h2 =+ orate*time*(RUNNING.getHrateinc())/100;
            RUNNING.setTotalHrateInc(h2);
            orate = nrate;
            calories = RUNNING.getCburnt()*time;
            sumcalories += calories;
            s2 += RUNNING.getCburnt()*time;
            RUNNING.setActCalories(s2) ;
        }
        
        else if("KICK_BOXING".equals(getChoice(choiceBox))){
            nrate =orate + orate*time*(KICK_BOXING.getHrateinc())/100;
            h3 += orate*time*(KICK_BOXING.getHrateinc())/100;
            KICK_BOXING.setTotalHrateInc(h3);
            orate = nrate;
            calories = KICK_BOXING.getCburnt()*time;
            sumcalories += calories;
            s3 += KICK_BOXING.getCburnt()*time;
            KICK_BOXING.setActCalories(s3) ;
         }
        
        else if("STRENGTH_TRAINING".equals(getChoice(choiceBox))){
            nrate =orate + orate*time*(STRENGTH_TRAINING.getHrateinc())/100;  
            h4 += orate*time*(STRENGTH_TRAINING.getHrateinc())/100;
            STRENGTH_TRAINING.setTotalHrateInc(h4);
            orate = nrate;
            calories = STRENGTH_TRAINING.getCburnt()*time;  
            sumcalories += calories;
            s4 += STRENGTH_TRAINING.getCburnt()*time;
            STRENGTH_TRAINING.setActCalories(s4) ;
        }
        txt2.setText(Integer.toString(sumcalories) + " calories");
        txt3.setText(Float.toString(nrate) + " beat/minute");
        AlertBox.display("Done", "Activity Recorded Successfully" , 250 , 100);
        }
        
        else AlertBox.display("Error", "Please enter a positive integer in time field" , 300 , 100);
        }
        };
    
    public void Result(ChoiceBox<String> choiceBox , TextField txt1 , TextField txt2 , TextField txt3 ,Scene scene2 , Stage window , ListView<String> list , Calculations b ){
           boolean flag = false ;
       if("".equals(choiceBox.getValue())) AlertBox.display("Error", "Please choose your activity" , 250 , 100);
        else if(isInt(txt1)){
        if(isPositive(txt1)){
            time = Integer.parseInt(txt1.getText());
            
       if(nrate == 0.0f){
           flag = true;
        if( "SWIMMING".equals(getChoice(choiceBox)) ){
            nrate = orate + orate*time*(SWIMMING.getHrateinc())/100;
            h1 += orate*time*(SWIMMING.getHrateinc())/100;
            SWIMMING.setTotalHrateInc(h1);
            orate = nrate;
            calories = SWIMMING.getCburnt()*time;
            sumcalories += calories;
            s1 += SWIMMING.getCburnt()*time;
            SWIMMING.setActCalories(s1) ;            
        }
        
        else if("RUNNING".equals(getChoice(choiceBox))){
            nrate = orate + orate*time*(RUNNING.getHrateinc()/100);
            h2 += orate*time*(RUNNING.getHrateinc())/100;
            RUNNING.setTotalHrateInc(h2);
            orate = nrate;
            calories = RUNNING.getCburnt()*time;
            sumcalories += calories;
            s2 += RUNNING.getCburnt()*time;
            RUNNING.setActCalories(s2) ;
        }
        
        else if("KICK_BOXING".equals(getChoice(choiceBox))){
            nrate =orate + orate*time*(KICK_BOXING.getHrateinc())/100;
            h3 += orate*time*(KICK_BOXING.getHrateinc())/100;
            KICK_BOXING.setTotalHrateInc(h3);
            orate = nrate;
            calories = KICK_BOXING.getCburnt()*time;
            sumcalories += calories;
            s3 += KICK_BOXING.getCburnt()*time;
            KICK_BOXING.setActCalories(s3) ;
         }
        
        else if("STRENGTH_TRAINING".equals(getChoice(choiceBox))){
            nrate =orate + orate*time*(STRENGTH_TRAINING.getHrateinc())/100;  
            h4 += orate*time*(STRENGTH_TRAINING.getHrateinc())/100;
            STRENGTH_TRAINING.setTotalHrateInc(h4);
            orate = nrate;
            calories = STRENGTH_TRAINING.getCburnt()*time;  
            sumcalories += calories;
            s4 += STRENGTH_TRAINING.getCburnt()*time;
            STRENGTH_TRAINING.setActCalories(s4) ;
        }
       }
        txt2.setText(Integer.toString(sumcalories) + " calories");
        txt3.setText(Float.toString(nrate) + " beat/minute");
        if(flag) 
            AlertBox.display ("Done" , "Activity Recorded Successfully" ,250 , 100 );  
        b.Sorting(list);
        }
        else AlertBox.display("Error", "Please enter a positive integer in time field" , 300 , 100);
        }
        
        };
    
            //Method to check if the time input is an integer    
        private boolean isInt(TextField txt){
        try{
            int a = Integer.parseInt(txt.getText());
            return true ;
        }catch(NumberFormatException e){
            AlertBox.display("Error", "Please enter a positive integer in time field" , 300 , 100);
            return false;
        }
    }
        
            //Method to check if the time input is a positive integer     
         private boolean isPositive(TextField txt){
            int a = Integer.parseInt(txt.getText());
            if (a > 0)return true;
            else return false;
        }
         
            //Method to get the choice from the user
        private String getChoice(ChoiceBox<String> choiceBox){
            String choice = choiceBox.getValue();
            return choice;
    }

          
            //Method for sorting          
        private void Sorting (ListView<String> list){
            ArrayList<SortedAct> a = new ArrayList<SortedAct>(); 
            a.add(new SortedAct ("SWIMMING" ,SWIMMING.getActCalories() , SWIMMING.getTotalHrateInc() ));
            a.add(new SortedAct ("RUNNING" ,RUNNING.getActCalories() , RUNNING.getTotalHrateInc() ));
            a.add(new SortedAct ("KICK_BOXING" ,KICK_BOXING.getActCalories() , KICK_BOXING.getTotalHrateInc() ));
            a.add(new SortedAct ("STRENGTH_TRAINING" ,STRENGTH_TRAINING.getActCalories() , STRENGTH_TRAINING.getTotalHrateInc() ));
       
        Collections.sort(a, new Comparator<SortedAct>() {
            @Override
            public int compare(SortedAct o1, SortedAct o2) {
             int a = Integer.compare(o2.getcBurnt(), o1.getcBurnt());
             if(a==0)Float.compare(o2.getHrincrease(), o1.getHrincrease());
             return a;
            }
        });
        
            //Listing the sorted activities
        a.forEach(n -> {
        if(n.getcBurnt()!=0 && n.getHrincrease()!=0)
        list.getItems().addAll(n.getName() + ": \nCalories burnt: " + n.getcBurnt() + " calories" + "\nHeart rate increase: " + n.getHrincrease() + " beat/minute");
        });
}
        
        
            //Method for closing the program but first giving the user a second chance for making sure to exit
       
    }


