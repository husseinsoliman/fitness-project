
package fitness;
       //Creating class for making objects of the activities that have only their name and the results to sort them 
public class SortedAct {
    private String name;
    private int cBurnt;
    private float hrincrease;

       //Constructor
public SortedAct(String name, int cBurnt, float hrincrease) {
    this.name = name;
    this.cBurnt = cBurnt;
    this.hrincrease = hrincrease;
    }


    public String getName() {
        return name;
    }

    
    public void setName(String name) {
        this.name = name;
    }

    
    public int getcBurnt() {
        return cBurnt;
    }

    
    public void setcBurnt(int cBurnt) {
        this.cBurnt = cBurnt;
    }

    
    public float getHrincrease() {
        return hrincrease;
    }

    
    public void setHrincrease(float hrincrease) {
        this.hrincrease = hrincrease;
    }
}
