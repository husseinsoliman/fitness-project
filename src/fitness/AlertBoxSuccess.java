
package fitness;

import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;

         //Specific alert box to the button result to show (Activity Recorded Successfully)
public class AlertBoxSuccess {
    
 public static void display (String text , String message , Scene scene , Stage window){
         //Primary Stage
    Stage window1 = new Stage();
    window1.initModality(Modality.APPLICATION_MODAL);
    window1.setTitle(text);
    
         //Label for showing the message and Button for closing and switching to the second scene
    Label label = new Label(message);
    Button button = new Button("OK");
    button.setOnAction(e -> {
        window1.close();
        window.setScene(scene);
            });
    
         //Layout
    VBox layout = new VBox(10);
    layout.setAlignment(Pos.CENTER);
    layout.getChildren().addAll(label , button);
    
         //Scene
    Scene scene1 = new Scene(layout , 250 , 100);
    
         //Window Showing
    window1.setScene(scene1);
    window1.showAndWait();
            }
    
}
